> [!NOTE]

> Je teste les notes.

Ca ne marche pas sur Gitlab il semblerait...

# Sopra

Sopra is a Python library for dealing with word pluralization.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install foobar
```

## Usage

```python
import foobar

# returns 'locals'
foobar.pluralize('local')

# returns 'geese'
foobar.pluralize('goose')

# returns 'server'
foobar.singularize('servers')

```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.